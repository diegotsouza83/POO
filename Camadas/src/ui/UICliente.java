package ui;

import java.util.Scanner;

import controller.Fachada;
import excecoes.ClienteException;
import model.Cliente;

public class UICliente {
	
    private Fachada fachada = Fachada.getInstancia();	
	private Scanner s = new Scanner(System.in);
	
	public void exibirMenu(){
		System.out.println("Digite 1 para cadastrar");
		System.out.println("Digite 2 para pesquisar");
		int opcao = s.nextInt();
		
		switch (opcao){
		case 1: 
			cadastrarCliente();
			break;
		}
	}
	
	private void cadastrarCliente(){
		Cliente c = new Cliente();
		System.out.println("Digite o código");
	    c.setCodigo(s.nextInt());
	    System.out.println("Digite o nome");
	    c.setNome(s.next());
	    
	    try {
			fachada.cadastrarCliente(c);
		} catch (ClienteException e) {
			System.out.println(e.getMessage());
		}
		
	}
	

}
