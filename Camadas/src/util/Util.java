package util;

import controller.Fachada;
import excecoes.ClienteException;
import model.Cliente;

public class Util {

	private static String[] nomes = new String[5];
	
	public static void inicializar(){
		
		
		Fachada fachada = Fachada.getInstancia();
		
		
//		for (int i = 0; i < 4; i++) {
//			Cliente c = new Cliente();
//			c.setCodigo(i);
//			c.setNome("Cliente " + i);
//			try {
//				fachada.cadastrarCliente(c);
//			} catch (ClienteException e) {
//				System.out.println("Erro ao cadastrar cliente " + i);
//			}
//		}
		
		Cliente c = new Cliente();
		c.setCodigo(0);
		c.setNome("Maria");
		
		try {
			Fachada.getInstancia().cadastrarCliente(c);
		} catch (ClienteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
