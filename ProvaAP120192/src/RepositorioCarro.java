
public class RepositorioCarro {
	Carro[] carroArray = new Carro[10000000];

	boolean cadastrarCarro(Carro c) {
		for (int i = 0; i < carroArray.length; i++) {
			if (carroArray[i] != null && carroArray[i].getMarca().equals(c.getMarca())) {
				int qtd = c.getQuantidade() + carroArray[i].getQuantidade();

				if (qtd > 1000) {
					carroArray[i].setQuantidade(1000);
				} else {
					carroArray[i].setQuantidade(qtd);
				}
				return true;
			}

		}

		for (int i = 0; i < carroArray.length; i++) {
			if (carroArray[i] == null) {
				carroArray[i] = c;
				return true;
			}
		}

		return false;
	}

	Carro[] consultarCarro(String tipo, String marca) {
		Carro[] resultado = null;
		int qtd = 0;

		for (int i = 0; i < carroArray.length; i++) {
			if (carroArray[i] != null && carroArray[i].getMarca().equals(marca)
					&& carroArray[i].getTipo().equals(tipo)) {
				qtd++;
			}
		}

		if (qtd > 0) {
			resultado = new Carro[qtd];
			int aux = 0;

			for (int i = 0; i < carroArray.length; i++) {
				if (carroArray[i] != null && carroArray[i].getMarca().equals(marca)
						&& carroArray[i].getTipo().equals(tipo)) {
					resultado[aux] = carroArray[i];
					aux++;
				}
			}

		}
		return resultado;

	}

	int removerCarro(String tipo, int quantidade) {
		int qtdRemovida = 0;
		if (quantidade > 0) {
			for (int i = 0; i < carroArray.length; i++) {
				if (carroArray[i] != null && carroArray[i].getTipo().equals(tipo)) {
					int qtd = quantidade - carroArray[i].getQuantidade();

					if (qtd <= 0) {
						qtdRemovida += carroArray[i].getQuantidade();
						carroArray[i].setQuantidade(0);
					} else {
						carroArray[i].setQuantidade(qtd);
						qtdRemovida += quantidade;
					}
				}
			}
			
			return qtdRemovida;
			
		} else {
			for (int i = 0; i < carroArray.length; i++) {
				if (carroArray[i] != null && carroArray[i].getTipo().equals(tipo)) {
					carroArray[i] = null;
					return -1;
				}

			}

		}
		return qtdRemovida;

	}
}
