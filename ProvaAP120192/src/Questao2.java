
public class Questao2 {
	public static void main(String[] args) {
		int i = 7, j = 9;

		System.out.println(++i + j);
		if (i == j) {
			j *= 2;
			i = ++j;
		}
		System.out.println(i);
		while (i < (j * j)) {
			i++;
		}

		System.out.println(i + --j);
		System.out.println(++j + --i + (j * i));

	}
}
