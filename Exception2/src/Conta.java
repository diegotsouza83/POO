
public class Conta {
	int numero;
	double saldo;
	
	public void debitar(double valor) throws ContaException {
		if (valor > saldo) {
			ContaException exception = 
					new ContaException(ContaException.CODE_SALDO_INFUFICIENTE);
			throw exception;
		} else if (numero > 0) {
			throw new ContaException(ContaException.CODE_CONTA_INVALIDA);
		} else if (numero > 1000) {
			throw new ContaException(ContaException.CODE_CONTA_BLOQUEADA);
		}
		
		
		saldo -= valor;
		
	}
	
	public void creditar (double valor){
		saldo +=valor;
	}

}
