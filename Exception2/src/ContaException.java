
public class ContaException extends Exception{
	public static final int CODE_SALDO_INFUFICIENTE = 1;
	public static final int CODE_CONTA_INVALIDA = 2;
	public static final int CODE_CONTA_BLOQUEADA = 3;
	private String exceptionMessage;
	
	public ContaException(int messageCode) {
		configurarMensagem(messageCode);
	}
	
	private void configurarMensagem(int code){
		switch (code) {
		case CODE_SALDO_INFUFICIENTE: 
			exceptionMessage = "Saldo insuficiente";
			break;
		case CODE_CONTA_INVALIDA:
			exceptionMessage = "Conta inválida";
			break;
		case CODE_CONTA_BLOQUEADA: 
			exceptionMessage = "Conta bloqueda";
			break;
		default:
			exceptionMessage = "Operação inválida";
			break;
		}
	}

}
