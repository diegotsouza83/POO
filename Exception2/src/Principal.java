
public class Principal {
	
	public static void main(String[] args) {
		
		Carro c = new Carro(200);
		c.setMarca("VW");
		c.setModelo("Gol");
		
		try {
			c.acelerar(10);
			c.acelerar(20);
			c.acelerar(20);
			c.acelerar(20);
			c.acelerar(20);
			c.frear(5);
			c.frear(20);
			c.frear(10);
			c.frear(10);
			
		} catch (AceleracaoException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (FrearException ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		} finally {
			System.out.println("velocidade atual = " + c.getVelocidade());
		}
		
	}
	
}
