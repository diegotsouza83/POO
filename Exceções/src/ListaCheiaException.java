
public class ListaCheiaException extends Exception{

	private String mensagem;

	public ListaCheiaException(String message) {

		mensagem = message;
	}

	@Override
	public String getMessage() {
		return mensagem;
	}


}
