
public class ClienteException extends Exception{
	
	private int exceptionCode;
	private String exceptionMessage;
	public static final int EXCEPTION_MESMO_NOME = 1;
	public static final int EXCEPTION_MESMO_CPF = 2;
	public static final int EXCEPTION_MESMO_CODIGO = 3;
	
	public ClienteException(int exceptionCode){
		this.exceptionCode = exceptionCode;
		setmessage();
		
	}
	
	public ClienteException(String message) {
		super(message);
	}
	
	private void setmessage(){
		String message = "Erro genérico";
		
		switch (exceptionCode) {
		case EXCEPTION_MESMO_NOME: 
		    message = "Já existe cliente com o memso nome";
			break;
		case EXCEPTION_MESMO_CPF: 
			message = "Já existe cliente com o memso CPF";
			break;
		case EXCEPTION_MESMO_CODIGO:
			message = "Já existe cliente com o memso código";
			break;
		}
		
		exceptionMessage = message;
		
	}
	
	public String getExceptionMessage() {
		return exceptionMessage;
	}
	
	

}
