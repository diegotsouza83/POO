package ui;

import controlador.Fachada;
import exececoes.CarroException;
import modelo.Carro;
import repositorio.RepositorioCarro;

import java.util.Scanner;

public class UiCarro {

    static Scanner s = new Scanner(System.in);

    public void showMenu(){

        int opcao;

        s = new Scanner(System.in);


        System.out.println("Escolha uma opção \n1 - Inserir carro \n2 - Remover \n3 - Procurar carro");
        opcao = s.nextInt();

        switch (opcao){
            case 1:
                inserirCarro();
                break;
        }

    }

    private void inserirCarro(){
        Carro c = new Carro();
        c.setMarca("VW");
        c.setModelo("Gol");
        c.setPlaca("KKK-1234");

        try {
            Fachada.getInstancia().inserCarro(c);
        }catch (CarroException e){
            System.out.println(e.getMessage());
        }



    }
}
