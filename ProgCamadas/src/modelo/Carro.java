package modelo;

public class Carro {
    @Override
	public String toString() {
		return "Carro [codigo=" + codigo + ", modelo=" + modelo + ", marca=" + marca + ", placa=" + placa + "]";
	}

	private int codigo;
    private String modelo;
    private String marca;
    private String placa;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }
}
