
public interface IRepositorio {
	
	public void inserir(Funcionario f);
	public Funcionario procurar(int id);

}
