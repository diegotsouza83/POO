package br.edu.fbv.camadas.repositorio;

import br.edu.fbv.camadas.excecoes.ClienteException;
import br.edu.fbv.camadas.model.Cliente;

public interface IRepositorioCliente {
	
	public void inserirCliente(Cliente c) throws ClienteException;
	public void removerCliente(Cliente c) throws ClienteException;
	public Cliente procurarCliente(String nome);

}
