
public class Conta {
	
	private String numero;
	private double saldo;
	static int valor;
	
	Conta(String numeroConta){
		numero = numeroConta;
	}
	
	Conta(String numeroConta, double saldoInicial){
		numero = numeroConta;
		saldo = saldoInicial;
	}
	
	Conta(){
		
	}
	
	public double getSaldo(){
		return this.saldo;
	}
	
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	void creditar(double valor){
		saldo = saldo + valor;
	}
	
	boolean debitar(double valor){
		if (saldo - valor >= 0){
			saldo = saldo - valor;
			return true;
		} else {
			return false;
		}
	}

}
