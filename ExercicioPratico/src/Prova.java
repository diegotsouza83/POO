public class Prova {
	public static void main(String[] args) {
		int i = 5, j = 6;

		System.out.println(++i + j);
		if (i == j) {
			j *= 3;
			i = ++j;
		}
		System.out.println(i);
		while (i < (j * j)) {
			i++;
		}

		System.out.println(i + --j);
		System.out.println(++j + --i + (j * i));

	}
}
