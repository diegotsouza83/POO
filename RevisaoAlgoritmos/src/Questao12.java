import java.util.Scanner;

public class Questao12 {

    static int contPares(int[][] matriz){
        int cont = 0;

        for (int i = 0; i < matriz.length; i ++){
            for (int j = 0; j < matriz.length; j ++) {
                if (matriz[i][j] % 2 == 0) {
                    cont++;
                }
            }
        }

        return cont;

    }

    static int contImpares(int[][] matriz){
        int cont = 0;

        for (int i = 0; i < matriz.length; i ++){
            for (int j = 0; j < matriz.length; j ++) {
                if (matriz[i][j] % 2 != 0) {
                    cont++;
                }
            }
        }

        return cont;

    }

    public static void main(String[] args) {
        int matriz [][] = new int [3] [3];

        Scanner s = new Scanner(System.in);

        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++) {
                System.out.println("Digite [" + i + "][" + j + "]");
                matriz[i][j] = s.nextInt();
            }
        }

        System.out.println("Quantida de pares = " + contPares(matriz) );

        System.out.println("Quantida de impares = " + contImpares(matriz) );
    }
}
