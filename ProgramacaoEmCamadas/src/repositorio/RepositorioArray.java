package repositorio;

import exececoes.CarroException;
import modelo.Carro;

import java.util.ArrayList;
import java.util.List;

public class RepositorioArray implements IRepositorio {
    private List<Carro> listaCarro;
    private static RepositorioArray instancia;

    public static RepositorioArray getInstancia(){
        if (instancia == null){
            instancia = new RepositorioArray();
        }

        return instancia;
    }

    private RepositorioArray(){
        listaCarro = new ArrayList<Carro>();
    }

    @Override
    public void inserCarro(Carro carro) throws CarroException {
        listaCarro.add(carro);
    }

    @Override
    public Carro procurarCarroPelaPlaca(String placa) {

        for (Carro c : listaCarro){
            if (c!=null && c.getPlaca().equals(placa)){
                return c;
            }
        }
        return null;
    }

    @Override
    public void removerCarro(String placa) throws CarroException {
    	for (Carro c : listaCarro){
            if (c!=null && c.getPlaca().equals(placa)){
                listaCarro.remove(c);
                return;
            }
        }
    }

    @Override
    public boolean existeCarro(String placa) {
        for (Carro c: listaCarro){
            if (c!=null && c.getPlaca().equals(placa)){
                return true;
            }
        }
        return false;
    }

	@Override
	public boolean isDatabaseOnline() {
		return true;
	}

	@Override
	public void atualizarCarro(Carro c) throws CarroException {
		 for (Carro carro : listaCarro) {
			 if (c.getCodigo() == carro.getCodigo()) {
				 carro = c;
				 return;
			 }
		 }
	}


}
