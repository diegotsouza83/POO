package controlador;

import exececoes.CarroException;
import modelo.Carro;

public class Fachada implements IControlador {

    private IControlador controladorCarro;
    private static Fachada instancia;

    public static Fachada getInstancia(){
        if(instancia == null){
            instancia = new Fachada();
        }
        return instancia;
    }

    private Fachada(){
        controladorCarro = ControladorCarro.getInstancia();
    }

    @Override
    public void inserCarro(Carro c) throws CarroException {
        controladorCarro.inserCarro(c);
    }

    @Override
    public Carro procurarCarroPelaPlaca(String placa) throws CarroException{
        return controladorCarro.procurarCarroPelaPlaca(placa);
    }

    @Override
    public void removerCarro(String placa) throws CarroException {
        controladorCarro.removerCarro(placa);

    }

	@Override
	public void atualizarCarro(Carro c) throws CarroException {
		controladorCarro.atualizarCarro(c);
	}
}
