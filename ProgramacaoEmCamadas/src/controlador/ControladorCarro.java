package controlador;

import exececoes.CarroException;
import modelo.Carro;
import repositorio.IRepositorio;
import repositorio.RepositorioArray;
import repositorio.RepositorioPostgres;

public class ControladorCarro implements IControlador {
    private static IRepositorio repositorioCarro;

    private static ControladorCarro instancia;

    protected static ControladorCarro getInstancia(){
        if (instancia == null) {
            instancia = new ControladorCarro();
        }
        
        if (RepositorioPostgres.getInstancia().isDatabaseOnline()) {
        	repositorioCarro = RepositorioPostgres.getInstancia();
        } else {
        	repositorioCarro = RepositorioArray.getInstancia();
        }

        return instancia;
    }

    private ControladorCarro(){
    }

    @Override
    public void inserCarro(Carro c) throws CarroException {
    	
    	if (c == null) {
    		throw new CarroException("Carro inválido");
    	}

        if (repositorioCarro.existeCarro(c.getPlaca())){
            throw new CarroException("Placa já existe");
        }
        repositorioCarro.inserCarro(c);

    }

    @Override
    public Carro procurarCarroPelaPlaca(String placa) throws CarroException {
    	if (placa == null || placa.isEmpty()) {
    		throw new CarroException("PLaca inválida");
    	}
        return repositorioCarro.procurarCarroPelaPlaca(placa);
    }

    @Override
    public void removerCarro(String placa) throws CarroException {
    	if (placa == null || placa.isEmpty()) {
    		throw new CarroException("PLaca inválida");
    	}
    	
    	if (!repositorioCarro.existeCarro(placa)) {
    		throw new CarroException("Não existe nenhum carro com a placa " + placa);
    	}
    	
    	repositorioCarro.removerCarro(placa);

    }

	@Override
	public void atualizarCarro(Carro c) throws CarroException {
		if (c == null) {
    		throw new CarroException("Carro inválido");
    	}

        if (!repositorioCarro.existeCarro(c.getPlaca())){
            throw new CarroException("Nenhum carro com a placa " + c.getPlaca());
        }
        
        repositorioCarro.atualizarCarro(c);
		
	}
}
