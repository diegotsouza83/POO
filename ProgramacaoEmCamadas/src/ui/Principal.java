package ui;

import java.util.Scanner;

import controlador.ControladorCarro;

public class Principal {

    public static void main(String[] args) {
        int opcao = 0;

        Scanner s = new Scanner(System.in);

        do {
        	 System.out.println("Escolha uma opção \n1 - Cadastro de carros \n2 - Cadastro usuário \n0 - Sair");
             opcao = s.nextInt();

             switch (opcao){
                 case 1:
                     UiCarro uiCarro = new UiCarro();
                     uiCarro.showMenu();
                     break;
                 case 0: 
                	 System.out.println("Obrigado por utilizar o sistema");
                	 break;
             }
        }while (opcao != 0);

       
    }
}
