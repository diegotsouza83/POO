
public class RepositorioCliente {
	
	private Cliente[] listaCliente;
	private static RepositorioCliente instancia;
	
	private RepositorioCliente(){
		listaCliente = new Cliente[100];
	}
	
	public static RepositorioCliente getInstancia(){
		if (instancia == null) {
			instancia = new RepositorioCliente();
		}
		return instancia;
	}
	
	public boolean inserirCliente(Cliente c){
		
		for (int i = 0; i < listaCliente.length; i++) {
			if (listaCliente[i] != null && listaCliente[i].getCpf().equals(c.getCpf()) && 
					listaCliente[i].getNome().equals(c.getNome())){
				return false;
			}
			
		}
		for (int i = 0; i < listaCliente.length; i++) {
			if (listaCliente[i] == null) {
				listaCliente[i] = c;
				return true;
			}
		}
		
		return false;
	}

}
