
public class RepositorioArray implements IRepositorio {
	
	private Cliente[] listaClientes;
	private static RepositorioArray instancia;
	
	private RepositorioArray() {
		listaClientes = new Cliente[100];
	}
	
	public static RepositorioArray getInstancia(){
		if (instancia == null) {
			instancia = new RepositorioArray();
		}
		
		return instancia;
	}
	
	public void inserirCliente(Cliente c) throws Exception{
		int pos = -1;
		
		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] != null){
				if (listaClientes[i].id == c.id){
					throw new Exception("Cliente com id já cadastrado");
				}
			} else {
				pos = i;
			}
		}
		if (pos != -1) {
			listaClientes[pos] = c;
		} else {
			throw new Exception("Lista cheia");
		}
		
		
	}

}
