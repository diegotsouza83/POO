import java.util.ArrayList;
import java.util.List;

public class RepositorioClienteArrayList implements IRepositorio{
	private List<Cliente> listaCliente;
	private static RepositorioClienteArrayList instancia;
	
	public static RepositorioClienteArrayList getInstancia(){
		if(instancia == null) {
			instancia = new RepositorioClienteArrayList();
		}
		return instancia;
	}
	
	private RepositorioClienteArrayList() {
		listaCliente = new ArrayList<Cliente>();
	}
	
	public void inserirCliente(Cliente c) throws Exception{
		for (Cliente cliente : listaCliente) {
			if (cliente.id == c.id) {
				throw new Exception("Cliente já cadastrado");
			}
		}
		
		listaCliente.add(c);
		
	}
	
	public void teste(){
		
	}

}
