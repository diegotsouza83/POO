import java.util.Scanner;

public class Principal {

    static Scanner scanner = new Scanner(System.in);

    static RepositorioCliente repositorioCliente = RepositorioCliente.getInstancia();

    public static void main(String[] args) {
        int opcao = 0;

        do{
            opcao = exibirMenu();
        } while (opcao != 0);

    }

    static int exibirMenu(){
        int opcao;
        System.out.println("Bem Vindo ao Sistema de cadastro\n" +
                " 1 - Cadastro de Clientes \n 2 - Cadastro de fornecedores \n 0 - Sair");

        opcao = scanner.nextInt();

        switch (opcao){
            case 0:
                System.out.println("Obrigado por utilizar o sistema");
                break;
            case 1:
                System.out.println("Cadastro de clientes");
                exibirMenuCliente();
                break;
            case 2:
                System.out.println("Cadastro de fornecedores");
                break;
            default:
                System.out.println("Opção inválida");
                opcao = 0;
                break;
        }

        return opcao;
    }

    static void exibirMenuCliente(){
        System.out.println("1 - Inserir Cliente\n2 - Remover Cliente pelo código\n3 - Remover Cliente pelo nome\n" +
                "4 - Pesquisar cliente pelo código\n5 - Pesquisar cliente pelo bairro");
        int opcao = scanner.nextInt();

        switch (opcao){
            case 1:
                cadastrarCliente();
                break;
            case 2 :
                removerClientePeloCodigo();
                break;
            case 3:
                removerClientePeloNome();
                break;
            case 4:
                pesquisarClientePeloCodigo();
                break;
            case 5:
                pesquisarClientePeloCodigoPorBairro();
                break;
            default:
                System.out.println("Opção inválida");
        }
    }

    static void cadastrarCliente(){
        Cliente c = new Cliente();
        System.out.println("Digite o nome do cliente");
        c.setNome(scanner.next());
        System.out.println("Digite o email do cliente");
        c.setEmail(scanner.next());
        System.out.println("Digite o telefone do cliente");
        c.setTelefone(scanner.next());
        System.out.println("Digite o CPF do cliente");
        c.setCpf(scanner.next());
        System.out.println("Digite a idade do cliente");
        c.setIdade(scanner.nextInt());
        System.out.println("Digite o código do cliente");
        c.setCodigo(scanner.nextInt());

        Endereco endereco = new Endereco();

        System.out.println("Digite a rua");
        endereco.setRua(scanner.next());
        System.out.println("Digite o bairro");
        endereco.setBairro(scanner.next());

        c.setEndereco(endereco);

        boolean resultado = repositorioCliente.inserirCliente(c);

        if (resultado){
            System.out.println("Cliente cadastrado com sucesso");
        } else {
            System.out.println("Erro ao cadastrar o cliente");
        }


    }

    static void removerClientePeloCodigo(){

        System.out.println("Digite o código");

        int codigo = scanner.nextInt();

        boolean resultado = RepositorioCliente.getInstancia().removerCliente(codigo);

        if (resultado){
            System.out.println("Cliente removido com sucesso");
        } else {
            System.out.println("Nenhum cliente removido");
        }
    }

    static void removerClientePeloNome(){
        System.out.println("Digite o nome");
        String nome = scanner.next();

        int resultado = repositorioCliente.removerClientePeloNome(nome);

        if (resultado == 0) {
            System.out.println("Nenhum cliente removido");
        } else {
            System.out.println("Sucesso. " + resultado + " clientes removidos");
        }
    }

    static void pesquisarClientePeloCodigo(){
        System.out.println("Digite o código");
        int codigo = scanner.nextInt();

        Cliente c = repositorioCliente.buscarClientePorCodigo(codigo);

        if (c!= null){
            System.out.println("Nome do cliente = " + c.getNome());
        } else {
            System.out.println("Não existe cliente cadastrado");
        }
    }

    static void pesquisarClientePeloCodigoPorBairro(){

        System.out.println("Digite o nome do bairro");
        String bairro = scanner.next();

        Cliente[] clientes = repositorioCliente.buscarClientesPorBairro(bairro);

        if (clientes != null && clientes.length > 0) {
            System.out.println("Clientes no bairro " + bairro);
            for (int i = 0; i < clientes.length; i++) {
                if (clientes[i] != null)
                System.out.println(clientes[i].getNome());

            }
        } else {
            System.out.println("Não existem clientes no bairro " + bairro);
        }

    }
}
