
public class RepositorioAluno {
	
	private Aluno[] listaAluno  = new Aluno[100];
	
	public boolean cadastrarAluno(Aluno aluno){
		for (int i = 0; i < listaAluno.length; i++) {
			if (listaAluno[i] == null){
				listaAluno[i] = aluno;
				return true;
			} 
		}
		return false;
	}

	public boolean removerAluno(String login){
		for (int i = 0; i < listaAluno.length; i++) {
			if (listaAluno[i] != null && 
					listaAluno[i].getLogin().equals(login)){
				listaAluno[i] = null;
				return true;
			}
		}
		return false;
	}
	
	public Aluno pesquisarAlunoPeloLogin(String login) {
		for (int i = 0; i < listaAluno.length; i++) {
			if (listaAluno[i] != null && listaAluno[i].getLogin().equals(login)){
				return listaAluno[i];
			}
		}
		
		return null;
	}
	

}
