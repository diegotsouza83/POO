
public class Carro {
	
	String marca;
	String cor;
	String placa;
	String modelo;
	double velocidade;
	
	void acelerar(double valor){
		velocidade += valor;
	}
	 
	boolean freiar(double valor){
		if (valor <= velocidade) {
			velocidade -= valor;
			return true;
		}
		return false;
	}

}
