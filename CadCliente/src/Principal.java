import java.util.Scanner;

public class Principal {
	
	static Scanner s = new Scanner(System.in);
	static RepositorioCliente repositorio = RepositorioCliente.getInstancia();
	
	public static void main(String[] args) {
		int opcao = 0;
		do {
			
			System.out.println("Cadastro de clientes");
			System.out.println("1-Cadastrar\n2-Consultar\n3-Remover\n4-Sair");
			System.out.println("Digite uma opção");
			opcao = s.nextInt();
		
			switch (opcao){
			case 1: 
				inserirCliente();
				break;
			case 2: 
				consultarCliente();
				break;
			case 3: 
				removerCliente();
				break;
			case 4:
				System.out.println("Obrigado por utilizar o sistema");
				break;
			default:
				System.out.println("Opção inválida");
			}
		} while (opcao != 4);
		
	}

    private static void consultarCliente() {
    	int codigo;
	    System.out.println("Digite o código do cliente a ser procurado");
	    codigo = s.nextInt();
	    
	    Cliente c = repositorio.procurarCliente(codigo);
	    
	    if (c!=null) {
	    	System.out.println("nome = " + c.getNome());
	    	System.out.println("email = " + c.getEmail());
	    } else {
	    	System.out.println("Cliente inexistente");
	    }
		
	}

	private static void removerCliente() {
		int codigo;
	    System.out.println("Digite o código");
	    codigo = s.nextInt();
	    
	    repositorio = RepositorioCliente.getInstancia();
	    
	    if (repositorio.removerCliente(codigo)) {
	    	System.out.println("Cliente removido");
	    } else {
	    	System.out.println("Cliente inexistente");
	    }
		
	}

	static void inserirCliente() {
    	Cliente c = new Cliente();
    	System.out.println("Digite o código");
    	c.setCodigo(s.nextInt());
    	System.out.println("Digite o nome");
    	c.setNome(s.next());
    	System.out.println("Digite o endereço");
    	c.setEndereco(s.next());
    	System.out.println("Digite o email");
    	c.setEmail(s.next());
    	
    	if (repositorio.inserirCliente(c) == true) {
    		System.out.println("Cliente cadastrado");
    	} else {
    		System.out.println("Cliente não cadastrado");
    	}
    
		
	}

}
