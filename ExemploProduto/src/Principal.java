import java.util.Scanner;

/*
 * Criar uma classe produto com os atributos (identificador, nome, modelo e tipo)

Criar uma classe principal e criar 3 objetos 
produto com dados preenchidos pelo usuário

 */
public class Principal {
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		Produto p1 = new Produto();
		Produto p2 = new Produto();
		
		System.out.println("Digite o identificador do produto 1");
		p1.identificador = s.nextInt();
		System.out.println("Digite o modelo do produto 1");
		p1.modelo = s.next();
		System.out.println("Digite o tipo do produto 1");
		p1.tipo = s.next();
		System.out.println("Digite o nome do produto 1");
		p1.nome = s.next();
		
		System.out.println("Digite o identificador do produto 2");
		p2.identificador = s.nextInt();
		System.out.println("Digite o modelo do produto 2");
		p2.modelo = s.next();
		System.out.println("Digite o tipo do produto 2");
		p2.tipo = s.next();
		System.out.println("Digite o nome do produto 2");
		p2.nome = s.next();
		
		p1 = p2;
		
		System.out.println("O modelo do produto 1 é " + p1.modelo);
		System.out.println("O modelo do produto 2 é " + p2.modelo);
		
	}

}
