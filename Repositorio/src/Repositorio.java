
public class Repositorio {
	
	private Cliente[] listaCliente;
	
	public Repositorio(){
		listaCliente = new Cliente[1000];
	}
	
	public boolean cadastrarCliente(Cliente c){
		for (int i = 0; i < listaCliente.length; i++) {
			if (listaCliente[i] == null) {
				listaCliente[i] = c;
				return true;
			}
		}
		return false;
	}
	
	public boolean removerCliente(Cliente c){
		for (int i = 0; i < listaCliente.length; i++) {
			if (listaCliente[i].equals(c)){
				listaCliente[i] = null;
				return true;
			}
		}
		return false;
	}
	
	public Cliente[] listarClientesPorEndereco(String endereco){
		
		Cliente[] clientes = null;
		int cont = 0;
		int position = 0;
		
		for (int i = 0; i < listaCliente.length; i++) {
			if (listaCliente[i]!= null && listaCliente[i].getEndereco().equals(endereco)){
				cont++;
			}
		}
		
		if (cont > 0) {
			clientes = new Cliente [cont];
			for (int i = 0; i < listaCliente.length; i++) {
				if (listaCliente[i]!= null  && listaCliente[i].getEndereco().equals(endereco)){
					clientes[position] = listaCliente[i];
					position++;
				}
			}
		}
		
		
		return clientes;
		
	}

}
