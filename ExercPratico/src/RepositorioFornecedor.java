
public class RepositorioFornecedor {
	
	private static RepositorioFornecedor instancia;
	private Fornecedor[] listaFornecedor;
	
	private RepositorioFornecedor(){
		listaFornecedor = new Fornecedor[100];
	}
	
	public static RepositorioFornecedor getInstancia(){
		if (instancia == null) {
			instancia = new RepositorioFornecedor();
		}
		
		return instancia;
	}

}
