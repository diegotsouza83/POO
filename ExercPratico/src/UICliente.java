import java.util.Scanner;


public class UICliente {
	
	private static Scanner scanner = new Scanner(System.in);
	
	public void showMenu(){
		
		int opcao = 0;
		
		do{
			
			System.out.println("======Cadastro de clintes======");
			System.out.println("Escolha uma opção.\n1 - Inserir cliente\n2 - Remover cliente pelo cpf\n"
					+ "3 - Remover cliente pelo nome\n4 - Pesquisar cliente pelo cpf\n5- Pesquisar cliente pelo bairro\n"
					+ "0 - Voltar ao menu principal");
			opcao = scanner.nextInt();
			
			switch(opcao){
				case 1: 
					inserirCliente();
					break;
				case 2: 
					removerClientePeloCPF();
					break;
				case 3: 
					removerClientePeloNome();
					break;
				case 4: 
					pesquisarClientePeloCPF();
					break;
				case 5: 
					pesquisarClientePeloBairro();
					break;
				case 0:
					break;
				default:
					System.out.println("Opção inválida");
					break;
			}
			
		}while (opcao != 0);
	}

	private void pesquisarClientePeloBairro() {
		String bairro;
		
		System.out.println("Digite o bairro");
		bairro = scanner.next();
		
		Cliente[] lista = RepositorioCliente.getInstancia().pesquisarClientePeloBairro(bairro);
		
		if (lista != null && lista.length > 0) {
			for (int i = 0; i < lista.length; i++) {
				System.out.println(lista[i].toString());
			}
		} else {
			System.out.println("Nenhum cliente encontrado");
		}
		
	}

	private void pesquisarClientePeloCPF() {
		String cpf;
		
		System.out.println("Digite o CPF");
		cpf = scanner.next();
		Cliente c = RepositorioCliente.getInstancia().pesquisarClientePeloCPF(cpf);
		if (c != null) {
			System.out.println(c.toString());
		} else {
			System.out.println("Cliente não encontrado");
		}
		
		
	}

	private void removerClientePeloNome() {
		String nome;
		System.out.println("Digite o nome a ser removido");
		nome = scanner.next();
		
		int qtd = RepositorioCliente.getInstancia().removerClientePeloNome(nome);
		
		System.out.println("Foram removidos " + qtd + " clientes com o nome " + nome);
		
	}

	private void removerClientePeloCPF() {
		String cpf;
		System.out.println("Digite o CPF a ser removido");
		cpf = scanner.next();
		
		if (RepositorioCliente.getInstancia().removerClientePeloCPF(cpf)){
			System.out.println("Cliente removido com sucesso");
		} else {
			System.out.println("Não existe cliente com o CPF informado");
		}
		
	}

	private void inserirCliente() {
		Cliente c = new Cliente();
		System.out.println("Digite o código");
		c.setCodigo(scanner.nextInt());
		System.out.println("Digite o nome");
		c.setNome(scanner.next());
		System.out.println("Digite o cpf");
		c.setCpf(scanner.next());
		System.out.println("Digite o email");
		c.setEmail(scanner.next());
		System.out.println("Digite a idade");
		c.setIdade(scanner.nextInt());
		System.out.println("Digite a data de nascimento");
		c.setDataNascimento(scanner.next());
		Endereco e = criarEndereco();
		c.setEndereco(e);
		
		if (RepositorioCliente.getInstancia().inserirCliente(c)) {
			System.out.println("Cliente inserido com sucesso");
		} else {
			System.out.println("Cliente não inserido");
		}
		
	}
	
	private Endereco criarEndereco(){
		System.out.println("Digite a rua");
		String rua = scanner.next();
		System.out.println("Digite o bairro");
		String bairro = scanner.next();
		System.out.println("Digite o número");
		int numero = scanner.nextInt();
		System.out.println("Digite o complemento");
		String complemento = scanner.next();
		System.out.println("Digite o CEP");
		String cep = scanner.next();
		
		
		Endereco e = new Endereco(rua, bairro, numero, complemento, cep);
		return e;
	}

	
	
	
	
	
	
	
	
}
