
public interface IRepositorio {
	public void cadastrarCliente(Cliente c);
	public void removerCliente(int codigo);
	public void buscarCliente(int codigo);

}
