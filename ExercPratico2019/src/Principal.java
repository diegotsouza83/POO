import java.util.Scanner;

public class Principal {

	static Scanner s = new Scanner(System.in);

	RepositorioCliente repositorioCliente = new RepositorioCliente();
	RepositorioFornecedor repositorioForncedor = new RepositorioFornecedor();

	public static void main(String[] args) {
		int opcao = 0;
		Principal p = new Principal();
		do {
			System.out.println("Escolha uma opção");
			System.out.println("1 - Cadastro de Clientes\n2 - Cadastro de fornecedores");
			opcao = s.nextInt();

			switch (opcao) {
			case 1:
				p.exibirMenuCliente();
				break;
			case 2:
				p.exibirFornecedor();
				break;
			default:
				opcao = 0;
			}
		} while (opcao != 0);

	}

	public void exibirMenuCliente() {
		int opcao = 0;

		System.out.println("MENU CLIENTE");
		System.out.println("1 - Inserir cliente");
		System.out.println("2 - Remover cliente pelo código");
		System.out.println("3 - Remover cliente pelo nome");
		System.out.println("4 - Pesquisar cliente pelo código");
		System.out.println("5 - Pesquisar cliente pelo bairro");

		opcao = s.nextInt();

		switch (opcao) {
		case 1:
			cadastrarCliente();
			break;
		case 2:
			removerClientePeloCodigo();
			break;
		case 3:
			removerClientePeloNome();
			break;
		case 4:
			pesquisarClientePeloCodigo();
			break;
		case 5:
			pesquisarClientesPeloBairro();
			break;
		default:
			System.out.println("Opção inválida");
		}

	}
	
	public void exibirFornecedor() {
		int opcao = 0;

		System.out.println("MENU CLIENTE");
		System.out.println("1 - Inserir fornecedor");
		System.out.println("2 - Remover fornecedor pelo CNPJ");
		System.out.println("3 - Remover fornecedor pelo tipo de serviço");
		System.out.println("4 - Pesquisar fornecedor pelo CEP");

		opcao = s.nextInt();

		switch (opcao) {
		case 1:
			inserirFornecedor();
			break;
		case 2:
			removerFornecedor();
			break;
		case 3:
			pesquisarFornecedorTipoDeServico();
			break;
		case 4:
			pesquisarFornecedorCEP();
			break;
		default:
			System.out.println("Opção inválida");
		}

	}
	
	public void cadastrarCliente() {
		Cliente c = new Cliente();
		System.out.println("Digite o nome");
		c.setNome(s.next());
		System.out.println("Digite o email");
		c.setEmail(s.next());
		System.out.println("Digite o código");
		c.setCodigo(s.nextInt());
		System.out.println("Digite o CPF");
		c.setCpf(s.next());
		System.out.println("Digite a idade");
		c.setIdade(s.nextInt());

		Endereco e = new Endereco();
		System.out.println("Digite a rua");
		e.setRua(s.next());
		System.out.println("Digite o bairro");
		e.setBairro(s.next());
		System.out.println("Digite o CEP");
		e.setCep(s.next());
		System.out.println("Digite o número");
		e.setNumero(s.nextInt());

		c.setEndereco(e);

		if (repositorioCliente.inserirCliente(c) == true) {
			System.out.println("Cliente cadastrado com sucesso");
		} else {
			System.out.println("Erro ao cadastrar cliente");
		}

	}

	public void removerClientePeloCodigo() {
		int codigo;

		System.out.println("Digite o código do cliente a ser removido");
		codigo = s.nextInt();

		if (repositorioCliente.removerClientePeloCodigo(codigo) == true) {
			System.out.println("Cliente removido");
		} else {
			System.out.println("Cliente não encontrado");
		}

	}

	public void removerClientePeloNome() {
		String nome;

		System.out.println("Digite o nome dos clientes a serem removido");
		nome = s.next();

		int qtdRemovida = repositorioCliente.removerClientePeloNome(nome);

		System.out.println("Quantidade = " + qtdRemovida);

	}

	public void pesquisarClientePeloCodigo() {
		int codigo;

		System.out.println("Digite o código do cliente a ser procurado");
		
		codigo = s.nextInt();
		
		Cliente c = repositorioCliente.pesquisarClientePeloCodigo(codigo);
		
		if (c != null) {
			System.out.println("Nome: " + c.getNome() + "\nCPF:" + c.getCpf());
		} else {
			System.out.println("Nenhum cliente foi encontrado");
		}

	}

	public void pesquisarClientesPeloBairro() {
		String bairro;
		System.out.println("Digite o bairro a ser pesquisado.");
		
		bairro = s.next();
		
		Cliente[] clientes = repositorioCliente.pesquisarClientesPeloBairro(bairro);
		
		if (clientes != null && clientes.length > 0) {
			for (int i = 0; i < clientes.length; i++) {
				System.out.println("Nome: " + clientes[i].getNome() + "\nCPF:" + clientes[i].getCpf());
			}
		} else {
			System.out.println("Nenhum cliente foi encontrado para o bairro pesquisado");
		}

	}
	
	public void inserirFornecedor(){
		
		Fornecedor f = new Fornecedor();
		System.out.println("Digite o nome");
		f.setNome(s.next());
		System.out.println("Digite o nome dantasis");
		f.setNomeFantasia(s.next());
		System.out.println("Digite o email");
		f.setEmail(s.next());
		System.out.println("Digite o CNPJ");
		f.setCnpj(s.next());
		System.out.println("Digite a data de fundação");
		f.setDataFundacao(s.next());
		System.out.println("Digite o tipo de serviço");
		f.setTipoServico(s.next());

		Endereco e = new Endereco();
		System.out.println("Digite a rua");
		e.setRua(s.next());
		System.out.println("Digite o bairro");
		e.setBairro(s.next());
		System.out.println("Digite o CEP");
		e.setCep(s.next());
		System.out.println("Digite o número");
		e.setNumero(s.nextInt());

		f.setEndereco(e);

		if (repositorioForncedor.inserirFornecedor(f) == true) {
			System.out.println("Fornecedor cadastrado com sucesso");
		} else {
			System.out.println("Erro ao cadastrar fornecedor");
		}
		
	}
	
	public void removerFornecedor(){
		String cnpj;

		System.out.println("Digite o CNPJ do forncedor a ser removido");
		cnpj = s.next();

		if (repositorioForncedor.removerFornecedorPeloCNPJ(cnpj) == true) {
			System.out.println("Fornecedor removido");
		} else {
			System.out.println("Fornecedor não encontrado");
		}
	}
	
	public void pesquisarFornecedorTipoDeServico(){
		String  tipoServico;

		System.out.println("Digite o tipo de serviço a ser procurado");
		
		tipoServico = s.next();
		
		Fornecedor f = repositorioForncedor.pesquisarFornecedorPeloTipoDeServico(tipoServico);
		
		if (f != null) {
			System.out.println("Nome: " + f.getNome() + "\nCNPJ:" + f.getCnpj());
		} else {
			System.out.println("Nenhum forncedor foi encontrado");
		}

	}
	
	public void pesquisarFornecedorCEP(){
		
		String cep;
		System.out.println("Digite o CEP a ser pesquisado.");
		
		cep = s.next();
		
		Fornecedor[] fornecedores = repositorioForncedor.pesquisarFornecedorPeloCEP(cep);
		
		if (fornecedores != null && fornecedores.length > 0) {
			for (int i = 0; i < fornecedores.length; i++) {
				System.out.println("Nome: " + fornecedores[i].getNome() + "\nCNPJ:" + fornecedores[i].getCnpj());
			}
		} else {
			System.out.println("Nenhum fornecedor foi encontrado para o CEP pesquisado");
		}
		
	}

}
